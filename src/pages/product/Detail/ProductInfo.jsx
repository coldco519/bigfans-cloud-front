/**
 * Created by lichong on 2/22/2018.
 */

import React from 'react';
import {Row, Col, Card , InputNumber , Button , message} from 'antd';
import CitySwitcher from '../../../components/CitySwitcher';
import Sku from '../../../components/Sku';

import HttpUtils from '../../../utils/HttpUtils'

class ProductInfo extends React.Component {

    state = {
        quantity : 1
    }

    addToCart(){
        let params = {
            prodId : this.props.product.id,
            quantity : this.state.quantity
        }
        HttpUtils.addItem(params , {
            success : function (resp) {
                if(resp.status == 200){
                    message.success("商品已加入购物车")
                }
            } , error : function (resp) {
                message.error("加入购物车失败")
            }
        })
    }

    changeQuantity (quantity) {
        this.setState({quantity});
    }

    render() {
        return (
            <div>
                <h3 style={{marginTop: '10px'}}>{this.props.product && this.props.product.name}</h3>
                <Card style={{marginTop: '10px'}}>
                    <Row>
                        <Col span={3}>
                            价格
                        </Col>
                        <Col span={21}>
                            ¥ {this.props.product && this.props.product.price}
                        </Col>
                    </Row>
                </Card>
                <Row style={{marginTop: '10px'}}>
                    <Col span={3}>
                        配 送 至
                    </Col>
                    <Col span={21}>
                        <CitySwitcher/>
                    </Col>
                </Row>
                <Sku prodId={this.props.prodId}/>
                <Row style={{marginTop: '10px'}}>
                    <Col span={3}>
                        数量
                    </Col>
                    <Col span={21}>
                        <InputNumber min={1} step={1} defaultValue={this.state.quantity} onChange={(e) => this.changeQuantity(e)}/>
                    </Col>
                </Row>
                <Row style={{marginTop: '10px'}}>
                    <Col span={21} offset={3}>
                        <Button type="primary" onClick={() => this.addToCart()}>加入购物车</Button>
                        <Button type="primary" style={{marginLeft: '20px'}}>立即购买</Button>
                    </Col>
                </Row>
            </div>
        );
    }
}
export default ProductInfo;