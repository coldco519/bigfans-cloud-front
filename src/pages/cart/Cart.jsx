import React, {Component} from 'react';

import {Row, Col , Divider  , Tabs , Card} from 'antd';
import TopBar from '../../components/TopBar';
import SearchBar from '../../components/SearchBar'
import CartItems from '../../components/CartItems';

import MyFavors from './MyFavors'

const TabPane = Tabs.TabPane;

class Cart extends Component {

    addProductToCart(prodId){
        this.cartItems.addItem(prodId)
    }

    render() {
        return (
            <div>
                <Row>
                    <Col span={3}/>
                    <Col span={18}>
                        <TopBar/>
                    </Col>
                    <Col span={3}/>
                </Row>
                <Divider style={{margin: '0'}}/>
                <Row>
                    <Col span={3}/>
                    <SearchBar hideCart={true}/>
                    <Col span={3}/>
                </Row>
                <Divider style={{margin: '0'}}/>
                <Row>
                    <Col span={3}/>
                    <Col span={18}>
                        <CartItems getInstance={(child) => this.cartItems = child}/>
                        <Row style={{marginTop : '20px'}}>
                            <Tabs defaultActiveKey="1">
                                <TabPane tab="猜你喜欢" key="1">
                                    <MyFavors addProductToCart={(prodId) => this.addProductToCart(prodId)}/>
                                </TabPane>
                                <TabPane tab="我的关注" key="2">Content of Tab Pane 2</TabPane>
                                <TabPane tab="最近浏览" key="3">Content of Tab Pane 3</TabPane>
                            </Tabs>
                        </Row>
                    </Col>
                    <Col span={3}/>
                </Row>
            </div>
        )
    }
}

export default Cart;