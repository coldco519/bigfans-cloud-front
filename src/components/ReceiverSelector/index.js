/**
 * Created by lichong on 2/22/2018.
 */

import React from 'react';
import {Row, Col, Radio, Divider} from 'antd';
import AddressForm from 'components/AddressForm'

import HttpUtils from '../../utils/HttpUtils'


const RadioGroup = Radio.Group;

class ReceiverSelector extends React.Component {

    state = {
        addresses : [],
        selectedAddressId : '',
        visible: false
    }

    showModal () {
        this.setState({
            visible: true,
        });
    }

    handleCancel (e){
        console.log(e);
        this.setState({
            visible: false,
        });
    }

    handleOk(address){
        let newAddresses = this.state.addresses;
        newAddresses.unshift(address);
        this.setState({
            visible: false,
            addresses : newAddresses,
            selectedAddressId : address.id
        });
    }

    componentDidMount(){
        let self = this;
        HttpUtils.getMyAddress({} , {
            success : function (resp) {
                let primaryAddress = resp.data.filter((address) => {
                    return address.isPrimary === true;
                })
                let selectedAddress = {};
                if(primaryAddress && primaryAddress.length >0 ){
                    selectedAddress = primaryAddress[0];
                } else {
                    selectedAddress = resp.data[0];
                }
                self.setState({addresses: resp.data , selectedAddressId : selectedAddress.id})
                self.props.setReceiverInfo(selectedAddress);
            }
        })
    }

    changeReceiver(e){
        this.props.setReceiverInfo(e.target.data);
        this.setState({selectedAddressId : e.target.data.id});
    }

    render() {
        return (
            <div>
                <Row>
                    <h3 className='pull-left'>收货人信息</h3>
                    <a className="pull-right" onClick={() => this.showModal()}>新建联系人</a>
                    <AddressForm visible={this.state.visible} handleCancel={()=>this.handleCancel()} handleOk={(e)=>this.handleOk(e)}/>
                </Row>
                <Row>
                    <RadioGroup value={this.state.selectedAddressId} onChange={(e) => this.changeReceiver(e)}>
                        {
                            this.state.addresses.map((address , index) => {
                                return (
                                    <div style={{marginTop : '20px'}} key={index}>
                                        <Radio key={address.id} value={address.id} data={address}>
                                            <span style={{marginLeft : '20px'}}>{address.consignee}</span>
                                            <span style={{marginLeft : '20px'}}>{address.mobile}</span>
                                            <span style={{marginLeft : '20px'}}>{address.detailAddress}</span>
                                        </Radio>
                                        <br/>
                                    </div>
                                    )
                            })
                        }
                    </RadioGroup>
                </Row>
                <Divider/>
            </div>
        );
    }
}

export default ReceiverSelector;